Meter Reading App:
A React and TypeScript application for tracking water meter readings. Features adding new readings and viewing past data. Includes Jest for testing and Storybook for component development.

Setup prerequisites: Node.js (v12+), npm;

Install dependencies: npm install

Start the app: npm start

Running Tests: npm test

Using Storybook: npm run storybook

Technologies: React.js, TypeScript, Jest and React Testing Library, Storybook