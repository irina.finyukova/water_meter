import React, { ReactNode, useEffect } from 'react';
import Container from '../../Layout/Container/Container';
import s from './Modal.module.scss';

type ModalProps = {
    children?: ReactNode;
    openModal: boolean;
    onClose: () => void;
    backgroundColor?: 'white' | 'blue';
}

const Modal: React.FC<ModalProps> = ({ children, openModal, onClose, backgroundColor = 'white' }) => {
    const backgroundColorClass = backgroundColor == 'blue' && s['blue']

    useEffect(() => {
        if (openModal) {
            document.body.style.overflow = 'hidden';
        }
        return () => {
            document.body.style.overflow = 'unset';
        };
    }, [openModal]);

    if (!openModal) return null;

    return (
        <>
            {
                openModal &&
                <div className={s.modal_window}>
                    <Container>
                        <div className={`${s.modal_content} ${backgroundColorClass}`}>
                            <button className={s.close_button} onClick={onClose}>
                                <img className={s.close_img} src='../close.png' alt='close' />
                            </button>
                            <div className={s.content}>
                                {children}
                            </div>
                        </div>
                    </Container>

                </div>
            }
        </>
    );
};


export default Modal;
