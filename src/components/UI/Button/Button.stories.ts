import { StoryObj, Meta } from '@storybook/react';
import Button from './Button';


const meta: Meta<typeof Button> = {
  component: Button,
  argTypes: {
    type: {
      control: { type: null },
    },
  },
};
export default meta;

type Story = StoryObj<typeof Button>;


export const Primary: Story = {
  args: {
    color: 'primary', 
  },
};

export const DisabledButton: Story = {
  args: {
    color: 'disabled_button', 
  },
};

