
import s from './Button.module.scss'; 

interface ButtonProps {
  text: string;
  type: "button" | "submit" | "reset";
  isDisabled?: boolean,
  color?: 'primary' | 'disabled_button';
}

function Button({ type, color = 'primary', text = "Click", isDisabled = false }: ButtonProps): JSX.Element {
  const buttonClass = isDisabled ? s['disabled_button'] : s[color];

  return (
      <button className={`${s.button} ${buttonClass}`} type={type} disabled={isDisabled}>{text}</button>
  );
}
export default Button;



