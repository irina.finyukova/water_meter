import s from './Input.module.scss'; 

interface InputProps {
  value?: string | number;
  placeholder?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>
  inputError?: boolean;
}

function Input({ value, onChange, placeholder = "Enter here", inputError }: InputProps) {
  const InputAdditionalClass = inputError && s["error"] 

  return (
      <input 
        className={`${s.input} ${InputAdditionalClass}`} 
        type="text"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
      />
  );
}

export default Input;
