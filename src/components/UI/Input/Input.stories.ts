// Input.stories.tsx
import { StoryObj, Meta } from "@storybook/react";
import Input from "./Input";

const meta: Meta<typeof Input> = {
  title: "Components/UI/Input",
  component: Input,

  argTypes: {
    value: {
      control: { type: "text" },
    },
    placeholder: {
      control: { type: "text" },
      defaultValue: "Enter here",
    },
    inputError: {
      control: { type: "boolean" },
    },

    onChange: {
      control: { type: null },
    },
  },
};

type Story = StoryObj<typeof Input>;

export default meta;

export const InputDefault: Story = {
  args: {
    value: "",
    placeholder: "Enter here",
    inputError: false,
  },
};

export const InputError: Story = {
  args: {
    ...InputDefault.args,
    inputError: true,
  },
};
