import React, { useState } from "react";
import { MeterReading } from "../../types";
import { formatDate } from "../../utils/formatDate";
import CardBackground from "../Layout/CardBackground/CardBackground";
import Container from "../Layout/Container/Container";
import Modal from "../UI/Modal/Modal";
import s from './PreviousReading.module.scss';

interface PreviousReadingProps {
  readings: MeterReading[],
}

const PreviousReading: React.FC<PreviousReadingProps> = ({ readings }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);


  const handleModalOpen = () => {
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  const createReadingListItems = (count?: number) => {
    const reversedReadings = [...readings].reverse().slice(0, count);
    return reversedReadings.map((reading) => (
      <Container>

<div className={s.reading_item} key={reading.value}>
        <p className={s.reading_value}>
          {reading.value}
        </p>
        <p className={s.reading_date}>
          {reading.date && formatDate(reading.date)}
        </p>
      </div>
      </Container>
 
    ));
  };

  const shouldShowModalButton = readings.length > 4;

  return (
    <>
      <CardBackground>
        <div className={s.wrapper}>
          <h2>Previous readings</h2>

          {readings.length > 0 ?
            <>
              <div>{createReadingListItems(5)}</div>
              <div className={s.button_wrapper}>
                {shouldShowModalButton && <button className={s.show_button} onClick={handleModalOpen}>Show more</button>}
              </div>
            </>
            :
            <>
            <div> Here you'll find your previous readings</div>
            <img className={s.card_image} src='../water_drop_sleeping.png' alt='sleeping_sun_image'></img>
            </>
          }

        </div>
      </CardBackground>

      <Modal openModal={isModalOpen} onClose={handleModalClose}>
        {createReadingListItems()}
      </Modal>
    </>
  );
}

export default PreviousReading;
