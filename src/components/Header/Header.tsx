import Container from '../Layout/Container/Container';
import s from './Header.module.scss';

function Header() {

    return (
        <div className={s.header_wrapper}>
            <Container>
                <div className={s.flex_wrapper}>
                    <h1>Meter Readings</h1>
                </div>

            </Container>

        </div>
    )
}

export default Header
