import { ReactNode } from 'react';
import  s  from './CardBackground.module.scss';


type CardBackgroundProps = {
    children: ReactNode;
}

const CardBackground: React.FC<CardBackgroundProps> = ({ children }) => {
    return (
        <div className={s.wrapper}>
            {children}
        </div>
    );
};

export default CardBackground;
