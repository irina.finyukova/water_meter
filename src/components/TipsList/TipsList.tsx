import { useState } from 'react'
import CardBackground from '../Layout/CardBackground/CardBackground';
import Modal from '../UI/Modal/Modal'
import s from './TipsList.module.scss';
import { Tip } from "../../types";

interface TipsListItemProps {
    tip: Tip;
}

function TipsList() {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const tips = [
        {
            header: 'Use a Water Meter',
            text: 'Monitor your water usage with a meter. This can help you identify unusually high usage, which might indicate a leak or an area where you can cut back.',
            imagePath: '../water_drop.png'
        },
        {
            header: 'Fix Leaks Promptly',
            text: 'Even a small drip from a leaky faucet can waste a significant amount of water over time. Regularly check for leaks in faucets, pipes, and toilets, and repair them promptly.',
            imagePath: '../water_drop_plumber.png'
        },
        {
            header: 'Full Loads for Dishwashers and Washing Machines',
            text: 'Run your dishwasher and washing machine only with full loads. This maximizes efficiency and reduces the number of cycles you run.',
            imagePath: '../water_drop_dish.png'
        },
        {
            header: 'Limit Shower Time:',
            text: ' Reduce the duration of your showers. Even cutting back by a few minutes can save gallons of water.',
            imagePath: '../water_drop.png'
        },
     
        
    ];


    const handleModalOpen = () => {
        setIsModalOpen(true);
    };

    const handleModalClose = () => {
        setIsModalOpen(false);
    };


    const TipsListItem: React.FC<TipsListItemProps> = ({ tip }) => {
        return (
            <div className={s.tips_list_item}>
                <CardBackground>
                    <div  className={s.tip_header}>{tip.header}</div>
                    <div className={s.tip_wrapper}>
                        <div className={s.tip_text}>{tip.text}</div>
                        <img className={s.tips_image} src={tip.imagePath} alt={tip.header} />
                    </div>

                </CardBackground>

            </div>
        );
    };


    return (
        <>
            <div className={s.button_wrapper}>
                <button onClick={handleModalOpen} className={s.hints_button}>Helpful hints for saving water you can find here</button>
            </div>
            <Modal openModal={isModalOpen} onClose={handleModalClose} backgroundColor='blue' >
                <div className={s.header_wrapper}>
                    <h1 className={s.tips_header}>How to lower water bills?</h1>
                </div>

                <div className={s.tips_grid}>
                    {tips.map((tip, index) => (
                        <TipsListItem key={index} tip={tip} />
                    ))}
                </div>
            </Modal>
        </>
    )
}

export default TipsList
