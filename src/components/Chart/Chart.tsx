import ReactECharts from 'echarts-for-react';
import { MeterReading } from "../../types";
import { useState, useEffect } from 'react'
import CardBackground from '../Layout/CardBackground/CardBackground';
import s from './Chart.module.scss';
import { formatDate } from '../../utils/formatDate';

interface ChartProps {
    readings: MeterReading[],
}

const Chart: React.FC<ChartProps> = ({ readings }) => {
    const [chartOptions, setChartOptions] = useState({});
 

    useEffect(() => {
        let recentReadings = readings.length >= 5 ? readings.slice(-5) : readings;

        const { differences, monthLabels } = calculateDifferencesAndLabels(recentReadings);
        
        if (differences && monthLabels) {
            setChartOptions(createChartOptions(differences, monthLabels));
        }
    }, [readings]);


  
    function calculateDifferencesAndLabels(recentReadings: MeterReading[]): { differences: number[], monthLabels: string[] } {
        let differences = [];
        let monthLabels = [];
    
        for (let i = 1; i < recentReadings.length; i++) {
            const currentReading = recentReadings[i];
            const previousReading = recentReadings[i - 1];
            const difference = Number(currentReading.value) - Number(previousReading.value);
    
            differences.push(difference);
    
            const monthLabel = currentReading.date ? formatDate(new Date(currentReading.date), true) : '';
            monthLabels.push(monthLabel);
        }
     
        return { differences, monthLabels };
    }
    



    function createChartOptions(differences: number[], monthLabels: string[]) {
        return {
            title: {
                text: ''
            },
            tooltip: {},
            legend: {
                data: ['Differences']
            },
            xAxis: {
                data: monthLabels
            },
            grid: {
                top: '10px',
                right: '0px',
                bottom: '20px',
                left: '13%'
            },
            yAxis: {},
            series: [{
                name: 'Difference',
                type: 'bar',
                color: '#4890bf',
                data: differences
            }]
        };
    } 


    return (
        <CardBackground>
            <div className={s.wrapper}>
                <h2>Monthly usage graph</h2>

                {readings.length > 1 ?
                    <div className={s.chart_wrapper}>
                        <ReactECharts
                            option={chartOptions}
                            style={{ height: '100%', maxWidth: '100%', width: '100%' }}
                            opts={{ renderer: 'svg' }}
                        />
                    </div>

                    :
                    <div> This graph will show the differences in your readings</div>
                }
            </div>

        </CardBackground>
    )
}

export default Chart
