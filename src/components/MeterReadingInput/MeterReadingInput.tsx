import React from 'react'
import Button from '../UI/Button/Button'
import Input from '../UI/Input/Input'
import { useState, useRef } from 'react';
import debounce from '../../utils/debounce';
import { validateNumber } from '../../utils/validateNumber';
import { MeterReading, LastReading } from "../../types";
import s from './MeterReadingInput.module.scss';
import CardBackground from '../Layout/CardBackground/CardBackground';

export interface MeterReadingInputProps {
    readings: MeterReading[],
    lastReading: LastReading,
    setReadings: React.Dispatch<React.SetStateAction<MeterReading[]>>;
}

const MeterReadingInput: React.FC<MeterReadingInputProps> = ({ readings, setReadings, lastReading }) => {
    const [value, setValue] = useState<string>("");
    const [isValid, setIsValid] = useState<boolean>(true);
    const [isLower, setIsLower] = useState<boolean>(true);
    const [readingAdded, setReadingAdded] = useState<boolean>(false);
    const [inputErrorStatus, setInputErrorStatus] = useState<boolean>(false);

    let lastReadingFormated;

    if (lastReading !== undefined && lastReading < 10000) {
        lastReadingFormated = lastReading.toString().padStart(5, "0");
    }


    const debounceRef = useRef(debounce( (inputValue: string) => {
        const isValidNumber = validateNumber(inputValue);
        setIsValid(isValidNumber);
        setInputErrorStatus(!isValidNumber);
    }, 600));

    const debouncedValidateNumber = debounceRef.current.debouncedFunction;
    const clearDebounce = debounceRef.current.clear;


    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputValue = e.target.value;
        setValue(inputValue);
        setIsLower(true)
        setReadingAdded(false)

        if (inputValue !== "") {
            debouncedValidateNumber(inputValue);

        } else {
            clearDebounce();
            setIsValid(true);
            setInputErrorStatus(false);
        }
    };



    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        clearDebounce();
        const isValidNumber = validateNumber(value);
        setIsValid(isValidNumber);
        setInputErrorStatus(!isValidNumber);

        if (isValidNumber) {

            if (lastReading !== undefined && Number(lastReading) >= Number(value)) {
                setIsLower(false);
            } else {

                const newReading: MeterReading = {
                    value: value,
                    source: "customer",
                    date: new Date()
                };

                setReadings([...readings, newReading]);
                setIsLower(true);
                setValue("");
                setReadingAdded(true)
            }
        }
    };


    return (

        <CardBackground>
            <div className={s.wrapper}>
                <h2> Enter a new reading</h2>
                <form onSubmit={handleSubmit}>
                    <div className={s.input_wrapper}>
                        <Input
                            value={value}
                            placeholder="Enter number"
                            onChange={handleChange}
                            inputError={inputErrorStatus}
                        />
                        {readingAdded && <div className={s.message}>Reading successfully added!</div>}
                        {!isValid && <div className={s.error_message}>Enter a 5-digit number from <span>00000 to 99999</span> for your new reading.</div>}
                        {!isLower && <div className={s.error_message}>Your current reading should be higher than the previous one: <span>{lastReadingFormated ? lastReadingFormated : lastReading}</span> </div>}
                    </div>

                    <div className={s.button_wrapper}>
                        {
                            isValid ?
                                <Button type="submit" text="Submit" /> :
                                <Button type="submit" text="Submit" isDisabled={true} />
                        }
                    </div>
                </form>

            </div>

        </CardBackground>


    )
}

export default MeterReadingInput
