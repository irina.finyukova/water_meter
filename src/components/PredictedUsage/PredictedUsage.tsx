import React from "react";
import { MeterReading, LastReading } from "../../types";
import { useState, useEffect } from 'react'
import { predictUsage } from "../../utils/predictUsage";
import s from './PredictedUsage.module.scss';
import CardBackground from "../Layout/CardBackground/CardBackground";

interface PredictedUsageProps {
  readings: MeterReading[],
  lastReading: LastReading;
}


const PredictedUsage: React.FC<PredictedUsageProps> = ({ readings, lastReading }) => {
  const [nextPredictableReading, setNextPredictableReading] = useState<number | string>();

  useEffect(() => {
    setNextPredictableReading(predictUsage(readings, lastReading))
  }, [readings, lastReading]);


  return (

    <CardBackground>
      <div className={s.wrapper}>
        <h2>Predicted usage next month</h2>
        <div className={s.text_wrapper}>
          {nextPredictableReading ?
            <h2 className={s.predicted_number}>{nextPredictableReading}</h2>
            :
            <p> Please continue recording readings. <br />A minimum of 4 is required for accurate predictions.</p>
          }
        </div>
        <img className={s.card_image} src='../water_drop_science.png' alt='lightning_image'></img>
      </div>

    </CardBackground>

  );
}

export default PredictedUsage;

