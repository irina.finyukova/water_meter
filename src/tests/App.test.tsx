
import { render, screen } from '@testing-library/react';
import App from '../App';
import '@testing-library/jest-dom';


// Finds components

test('renders the Header component', () => {
  render(<App />);
  expect( screen.getByText("Meter Readings")).toBeInTheDocument();
});

test('renders the MeterReadingInput component', () => {
  render(<App />);
  expect(screen.getByPlaceholderText('Enter number')).toBeInTheDocument();
});

test('renders the PredictedUsage component', () => {
  render(<App />);
  expect(screen.getByText('Predicted usage next month')).toBeInTheDocument();
});

test('renders the PreviousReading component', () => {
  render(<App />);
  expect(screen.getByText('Previous meter readings')).toBeInTheDocument();
});


// Checks that there are no error messages on initialization

test('initially does not display an error message', () => {
  render(<App />);
  const errorMessage = screen.queryByText('Enter a 5-digit number from 00000 to 99999 for your new reading.');
  expect(errorMessage).not.toBeInTheDocument();
});

test('initially does not display an error message', () => {
  render(<App />);
  const errorMessage = screen.queryByText('Your current reading should be higher than the previous one');
  expect(errorMessage).not.toBeInTheDocument();
});


