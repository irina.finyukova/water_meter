import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MeterReading } from "../types";
import PreviousReading from '../components/PreviousReading/PreviousReading';


test('renders the list of readings', () => {
    const mockReadings: MeterReading[] = [
        { value: '00250', source: "customer" },
        { value: '00350', source: "customer" },
        { value: '00400', source: "customer" }
    ];

    render(<PreviousReading readings={mockReadings} />);
    mockReadings.forEach((reading) => {
        const listItem = screen.getByText(`${reading.value} - ${reading.source}`);
        expect(listItem).toBeInTheDocument();
    });
});


test('renders the list of readings', () => {
    const mockReadings: MeterReading[] = [
        { value: '75643', source: "customer" },
        { value: '75777', source: "customer" },
        { value: '75888', source: "estimated" }
    ];

    render(<PreviousReading readings={mockReadings} />);
    mockReadings.forEach((reading) => {
        const listItem = screen.getByText(`${reading.value} - ${reading.source}`);
        expect(listItem).toBeInTheDocument();
    });
});
