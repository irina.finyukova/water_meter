import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import PredictedUsage from '../components/PredictedUsage/PredictedUsage';
import { MeterReading, LastReading } from "../types";
import { predictUsage } from '../utils/predictUsage';


const mockReadings: MeterReading[] = [
    { value: '40600', source: "customer" },
];
const mockLastReading: LastReading = 40400;

test('renders the PredictedUsage component', () => {
    render(<PredictedUsage readings={mockReadings} lastReading={mockLastReading} />);
    expect(screen.getByText("Predicted usage next month")).toBeInTheDocument();
});

test('returns undefined for less than 4 readings', () => {
    const readings: MeterReading[] = [
        { value: '00250', source: "customer" },
        { value: '00350', source: "customer" },
        { value: '00400', source: "customer" }
    ];

    const lastArrayValue = readings[readings.length - 1];
    const lastReading: LastReading = (Number(lastArrayValue.value))

    expect(predictUsage(readings, lastReading)).toBeUndefined();
});

test('calculates the correct usage for 4 readings', () => {
    const readings: MeterReading[] = [
        { value: '00100', source: "customer" },
        { value: '00250', source: "customer" },
        { value: '00350', source: "customer" },
        { value: '00400', source: "customer" }
    ];

    const lastReading: LastReading = (Number('00400'))

    expect(predictUsage(readings, lastReading)).toBe('00500');
});

test('calculates the correct usage for 4 readings', () => {
    const readings: MeterReading[] = [
        { value: '34000', source: "customer" },
        { value: '35600', source: "customer" },
        { value: '41000', source: "customer" },
        { value: '41100', source: "customer" }
    ];

    const lastReading: LastReading = 41100

    expect(predictUsage(readings, lastReading)).toBe('43466');
});

test('calculates the correct usage for more than 4 readings', () => {
    const readings: MeterReading[] = [
        { value: '10000', source: "customer" },
        { value: '34000', source: "customer" },
        { value: '35600', source: "customer" },
        { value: '41000', source: "customer" },
        { value: '41100', source: "customer" }
    ];

    const lastReading: LastReading = 41100

    expect(predictUsage(readings, lastReading)).toBe('43466');
});