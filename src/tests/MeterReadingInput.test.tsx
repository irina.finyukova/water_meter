import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import MeterReadingInput from '../components/MeterReadingInput/MeterReadingInput';
import { MeterReading, LastReading } from "../types";


const mockReadings: MeterReading[] = [
    { value: '40600', source: "customer" },
];
const mockLastReading: LastReading = 40509;
const mockSetReadings = () => {}

// Finds components

test('renders the Input component', () => {
    render(<MeterReadingInput readings={mockReadings} lastReading={mockLastReading} setReadings={mockSetReadings} />);
    expect(screen.getByPlaceholderText("Enter number")).toBeInTheDocument();
});


test('renders the Button component', () => {
    render(<MeterReadingInput readings={mockReadings} lastReading={mockLastReading} setReadings={mockSetReadings} />);
    expect(screen.getByRole('button')).toBeInTheDocument();
});




