import { validateNumber } from "../utils/validateNumber";

test("5 digit number", () => {
  expect(validateNumber("73558")).toBe(true);
});

test("5 digit number", () => {
  expect(validateNumber("73118")).toBe(true);
});

test("more than 5 digits", () => {
  expect(validateNumber("86725664")).toBe(false);
});

test("more than 5 digits", () => {
  expect(validateNumber("8672546465664")).toBe(false);
});

test("less than 5 digits", () => {
  expect(validateNumber("756")).toBe(false);
});

test("less than 5 digits", () => {
  expect(validateNumber("1")).toBe(false);
});

test("non-numeric characters", () => {
  expect(validateNumber("836n64")).toBe(false);
});

test("non-numeric characters", () => {
  expect(validateNumber("iufrbreg")).toBe(false);
});

test("empty string", () => {
  expect(validateNumber("")).toBe(false);
});
