export type MeterReadingSource = "estimated" | "customer";
export type LastReading = number | undefined;

export interface MeterReading {
  value: number | string;
  source: MeterReadingSource;
  date?: Date | undefined
}
export interface Tip {
  header: string;
  text: string;
  imagePath: string;
}