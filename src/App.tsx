import { useState, useEffect } from "react";
import { MeterReading } from "./types";
import './global.css';
import PredictedUsage from "./components/PredictedUsage/PredictedUsage";
import PreviousReading from "./components/PreviousReading/PreviousReading";
import MeterReadingInput from "./components/MeterReadingInput/MeterReadingInput";
import Header from "./components/Header/Header";
import Container from "./components/Layout/Container/Container";
import Chart from "./components/Chart/Chart";
import TipsList from "./components/TipsList/TipsList";


export default function App() {
  const [readings, setReadings] = useState<MeterReading[]>([]);
  const [lastReading, setLastReading] = useState<number>();

  useEffect(() => {
    if (readings.length > 0) {
      const lastArrayValue = readings[readings.length - 1];
      setLastReading(Number(lastArrayValue.value))
    }
  }, [readings.length])


  return (
    <div className='main_wrapper'>
      <Header />
      <Container>
        <div className='grid_wrapper'>
          <MeterReadingInput readings={readings} setReadings={setReadings} lastReading={lastReading} />
          <PredictedUsage readings={readings} lastReading={lastReading} />
          <PreviousReading readings={readings} />
          <Chart readings={readings}/>
        </div>
        <TipsList />
      </Container>
    
    </div>

  );
}
