export function formatDate(date: Date, monthOnly: boolean = false): string {
  let options: Intl.DateTimeFormatOptions;

  if (monthOnly) {
    options = { month: "short" };
  } else {
    options = { day: "numeric", month: "short", year: "numeric" };
  }
  return date.toLocaleDateString("en-US", options);
}
