export default function debounce<F extends (...args: string[]) => void>(fn: F, ms: number) {
    let timeout: ReturnType<typeof setTimeout> | undefined;

    const debouncedFunction = (...args: Parameters<F>) => {
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => fn(...args), ms);
    };

    const clear = () => {
        if (timeout) {
            clearTimeout(timeout);
        }
    };

    return { debouncedFunction, clear };
}