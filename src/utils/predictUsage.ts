import { MeterReading, LastReading } from "../types";

export function predictUsage( readings: MeterReading[], lastReading: LastReading): number | string | undefined {
  if (readings.length < 4) {
    return undefined;
  }

  let lastFourReadings = readings.slice(-4);
  let readingDifference = 0;

  for (let i = lastFourReadings.length - 1; i > 0; i--) {
    readingDifference +=
      Number(lastFourReadings[i].value) - Number(lastFourReadings[i - 1].value);
  }

  let averageReadingDifference = Math.floor(
    readingDifference / (lastFourReadings.length - 1)
  );

  let nextReading;
  if (lastReading !== undefined) {
    nextReading = lastReading + averageReadingDifference;
  }

  if (nextReading !== undefined) {
    nextReading = nextReading.toString().padStart(5, "0");
  }

  return nextReading;
}
