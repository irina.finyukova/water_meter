export const validateNumber = (inputValue: string): boolean => {
  if (inputValue == "00000") {
    return false;
  } else {
    const regularExpression = /^[0-9]{5}$/;
    return regularExpression.test(inputValue);
  }
};
